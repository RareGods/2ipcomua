﻿unit UaCom2ip;

interface

uses
  System.Net.HttpClient,
  System.Net.Socket,
  System.Classes,
  XSuperObject;

Type
  T2ipProviderClass = Class
  private
    Fip: String;
    Fname_ripe: String;
    Fname_rus: String;
    Fsite: String;
    Fas: String;
    Fip_range_start: String;
    Fip_range_end: String;
    Froute: String;
    Fmask: String;
  published
    [Alias('ip')]
    /// <summary> проверяемый IP адрес; </summary>
    property ip: String read Fip write Fip;
    [Alias('name_ripe')]
    /// <summary> официальное название провайдера в базе данных Internet Routing Registry (IRR); </summary>
    property name_ripe: String read Fname_ripe write Fname_ripe;
    [Alias('name_rus')]
    /// <summary> название провайдера/бренд на русском; </summary>
    property name_rus: String read Fname_rus write Fname_rus;
    [Alias('site')]
    /// <summary> сайт провайдера; </summary>
    property site: String read Fsite write Fsite;
    [Alias('as')]
    /// <summary> номер автономной системы провайдера; </summary>
    property &as: String read &Fas write &Fas;
    [Alias('ip_range_start')]
    /// <summary>  числовое значение (iptolong) первого IP адреса сети провайдера; </summary>
    property ip_range_start: String read Fip_range_start write Fip_range_start;
    [Alias('ip_range_end')]
    /// <summary> числовое значение (iptolong) последнего IP адреса сети провайдера; </summary>
    property ip_range_end: String read Fip_range_end write Fip_range_end;
    [Alias('route')]
    /// <summary>  сеть провайдера; </summary>
    property route: String read Froute write Froute;
    [Alias('mask')]
    /// <summary> маска сети провайдера. </summary>
    property mask: String read Fmask write Fmask;
  End;

  T2ipGeoClass = Class
  private
    Fip: String;
    Fcountry_code: String;
    Fcountry: String;
    Fcountry_rus: String;
    Fregion: String;
    Fregion_rus: String;
    Fcity: String;
    Fcity_rus: String;
    Flatitude: String;
    Flongitude: String;
    Fzip_code: String;
    Ftime_zone: String;
  published
    [Alias('ip')]
    /// <summary> проверяемый IP адрес;</summary>
    property ip: String read Fip write Fip;
    [Alias('country_code')]
    /// <summary> 2-х символьный идентификатор страны по стандарту ISO 3166-1; </summary>
    property country_code: String read Fcountry_code write Fcountry_code;
    [Alias('country')]
    /// <summary> название страны на английском языке;</summary>
    property country: String read Fcountry write Fcountry;
    [Alias('country_rus')]
    /// <summary>название страны на русском языке; </summary>
    property country_rus: String read Fcountry_rus write Fcountry_rus;
    [Alias('region')]
    /// <summary>название региона на английском языке; </summary>
    property region: String read Fregion write Fregion;
    [Alias('region_rus')]
    /// <summary> название региона на русском языке;</summary>
    property region_rus: String read Fregion_rus write Fregion_rus;
    [Alias('city')]
    /// <summary> название населенного пункта (города) на английском языке;</summary>
    property city: String read Fcity write Fcity;
    [Alias('city_rus')]
    /// <summary> название населенного пункта (города) на русском языке;</summary>
    property city_rus: String read Fcity_rus write Fcity_rus;
    [Alias('latitude')]
    /// <summary>  географическая широта;</summary>
    property latitude: String read Flatitude write Flatitude;
    [Alias('longitude')]
    /// <summary> географическая долгота;</summary>
    property longitude: String read Flongitude write Flongitude;
    [Alias('zip_code')]
    /// <summary> почтовый индекс;</summary>
    property zip_code: String read Fzip_code write Fzip_code;
    [Alias('time_zone')]
    /// <summary>часовой пояс. </summary>
    property time_zone: String read Ftime_zone write Ftime_zone;
  End;

  T2ipComUa = Class(TComponent)
  private const
    SERVER = 'http://api.2ip.com.ua/';
  private
    FHttp: THTTPClient;
  public
    Function Geo: T2ipGeoClass; overload;
    Function Geo(Const ip: TIPAddress): T2ipGeoClass; overload;
    Function Geo(Const ip: String): T2ipGeoClass; overload;

    Function provider: T2ipProviderClass; overload;
    Function provider(Const ip: TIPAddress): T2ipProviderClass; overload;
    Function provider(Const ip: String): T2ipProviderClass; overload;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Http: THTTPClient read FHttp write FHttp;
  End;

implementation

{ T2ipComUa }

constructor T2ipComUa.Create(AOwner: TComponent);
begin
  inherited;
  Http := THTTPClient.Create;
end;

destructor T2ipComUa.Destroy;
begin
  Http.Free;
  inherited;
end;

function T2ipComUa.Geo(const ip: String): T2ipGeoClass;
begin
  Result := Self.Geo(TIPAddress.Create(ip));
end;

function T2ipComUa.provider: T2ipProviderClass;
begin
  Result := Self.provider(TIPAddress.Any);
end;

function T2ipComUa.provider(const ip: TIPAddress): T2ipProviderClass;
const
  METHOD = 'provider.json?ip=';
var
  LIP: String;
begin
  if ip.Address <> ip.Any.Address then
    LIP := ip.Address;
  Result := T2ipProviderClass.FromJSON(Http.Get(SERVER + METHOD + LIP)
    .ContentAsString);
end;

function T2ipComUa.provider(const ip: String): T2ipProviderClass;
begin
  Result := Self.provider(TIPAddress.Create(ip));
end;

function T2ipComUa.Geo(const ip: TIPAddress): T2ipGeoClass;
const
  METHOD = 'geo.json?ip=';
var
  LIP: String;
begin
  if ip.Address <> ip.Any.Address then
    LIP := ip.Address;
  Result := T2ipGeoClass.FromJSON(Http.Get(SERVER + METHOD + LIP)
    .ContentAsString);
end;

function T2ipComUa.Geo: T2ipGeoClass;
begin
  Result := Self.Geo(TIPAddress.Any);
end;

end.
